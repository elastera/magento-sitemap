![alt text](http://www.elastera.com/wp-content/uploads/2015/06/elastera-logo-270x80.png "elastera")  
[www.elastera.com](http://www.elastera.com) Twitter: [@elastera](https://twitter.com/elastera)  

# Description
This magento module will allow you to other file names for the sitemap than just `sitemap.xml`  
The filter that was added is `/*/*sitemap*.xml`.  
With this filter you can create a separate sitemap for every store.  
All sitemap files can no coexist in one folder.  
This folder can be synced between web nodes to get the sitemap cloud conform.  
Please keep in mind that the used folder(s) may need to be created, if they are not yet existing.  

# Installation
## using composer
To use `composer.phar` to install the extension, add the following configuration to your `composer.json`:  
```
{
    "minimum-stability": "dev",
    "repositories": [
        {
            "type": "vcs",
            "url": "https://bitbucket.org/elastera/magento-sitemap"
        }
    ],
    "require": {
        "Elastera/Sitemapfilename": "1.0.*"
    }
}
```  
[https://getcomposer.org/download/](https://getcomposer.org/download/)

## using modman
To install the extension with [modman](https://github.com/colinmollenhour/modman) get the extension using  
`git clone git@bitbucket.org:elastera/magento-sitemap.git`  
Then execute `modman link ../vendor/elastera/magento-sitemap`

## download and move files
For a manual install you just need to copy two files into magento:  
`app/code/community/Elastera/Sitemapfilename/etc/config.xml` and  
`app/etc/modules/Elastera_Sitemapfilename.xml`
